### What is this repository for? ###

My little experiments and work based on the "Homie" library - a smart IoT library for managing, tracking, updating nodes using MQTT by Marvin Rogers (https://github.com/marvinroger/homie-esp8266)
Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

See instructions by Marvin Rogers at the original github homie repository. 
You will need:
1. Arduino or ESP8266
2. MQTT server i.e. mosquitto


### Who do I talk to? ###

Marvin Rogers for anything related to "homie"
Me for anything related to my specific code examples here in this repo.
