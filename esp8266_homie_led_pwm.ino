#include <Homie.h>

const int PIN_DIM = 0;
const int PIN_RESET = 14;
const int PWM_VALUE=63;


const int gamma_table[PWM_VALUE+1] = {
   15, 30, 60, 90, 120, 135, 150, 165, 180, 195, 210, 
   225, 240, 255, 285, 300, 315, 330, 345, 360, 375, 390, 405, 
   420, 435, 450, 465, 480, 510, 525, 540, 555, 570, 585, 600,
   615, 630, 645, 660, 675, 690, 705, 720, 735, 750, 765, 780, 795, 
   810, 825, 840, 855, 870, 885, 900, 915, 930, 945, 960, 975, 990, 
   1005, 1023};


HomieNode lightNode("light", "switch");


int brightnessTransform(int value) {

  
  int brightness=value;

  brightness = map(brightness,0,100,0,PWM_VALUE);
  brightness = constrain(brightness,0,PWM_VALUE);
  brightness = gamma_table[brightness];
  
  return brightness;
}


bool lightEffectHandler(const HomieRange& range, const String& value) {
   if (value == "fade") {
    Homie.getLogger() << "Lord Fader is on vacation !" << endl;
    return true;
   }
   else if (value == "label2") {
    // statements
   }
   else {
      Homie.getLogger() << "No valid effect specified" << endl;
      return false;
}
  }


bool lightOnHandler(const HomieRange& range, const String& value) {
  if (value != "true" && value != "false") return false;

  bool on = (value == "true");
  analogWrite(PIN_DIM, on ? 1024 : 0);
  lightNode.setProperty("on").send(value);
  Homie.getLogger() << "Light is " << (on ? "on" : "off") << endl;

  return true;
}

bool lightIntensityHandler(const HomieRange& range, const String& value) {
  
 if (value.toInt() == 0 || value.toInt() == 100) {
   bool on = (value.toInt() == 100);
   analogWrite(PIN_DIM, on ? 1024 : 0 );
   lightNode.setProperty("intensity").send(String(value));
   Homie.getLogger() << "Light is " << (on ? "on" : "off") << endl;
 return true;
 }
  
  else if (value.toInt() > 0 && value.toInt() < 100) {
    int brightness = value.toInt();
    analogWrite(PIN_DIM, brightnessTransform(brightness));
    lightNode.setProperty("intensity").send(String(brightness));
    Homie.getLogger() << "Light intensity set to: " << brightness << endl;
  return true;
  }
  
  else return false;
}

void setup() {
  Serial.begin(115200);
  Serial << endl << endl;
  pinMode(PIN_DIM, OUTPUT);
  pinMode(PIN_RESET, INPUT_PULLUP);

  Homie.setResetTrigger(PIN_RESET, LOW, 2000); // Configure PIN to trigger Configuration Mode

  Homie_setFirmware("awesome-led-dimmer", "1.0.0");

  lightNode.advertise("on").settable(lightOnHandler);
  lightNode.advertise("intensity").settable(lightIntensityHandler);
  lightNode.advertise("effect").settable(lightEffectHandler);

  Homie.setup();
}

void loop() {
  Homie.loop();
}


